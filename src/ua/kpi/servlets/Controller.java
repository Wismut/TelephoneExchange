package ua.kpi.servlets;


import org.apache.log4j.Logger;
import ua.kpi.commands.ICommand;
import ua.kpi.manager.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.kpi.manager.Message.IO_EXCEPTION;
import static ua.kpi.manager.Message.SERVLET_EXCEPTION;


public class Controller extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Controller.class);
    private ControllerHelper controllerHelper = ControllerHelper.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;
        try {
            ICommand command = controllerHelper.getCommand(request);
            page = command.execute(request, response);
        } catch (ServletException e) {
            logger.error(e.getMessage());
            request.setAttribute("messageError", Message.getInstance().getProperty(SERVLET_EXCEPTION));
        } catch (IOException e) {
            logger.error(e.getMessage());
            request.setAttribute("messageError", Message.getInstance().getProperty(IO_EXCEPTION));
        }
        response.sendRedirect(page);
    }
}