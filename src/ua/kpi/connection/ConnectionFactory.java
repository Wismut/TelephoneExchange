package ua.kpi.connection;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import ua.kpi.manager.Config;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory implements Serializable {
    private static final Logger logger = Logger.getLogger(ConnectionFactory.class);
    private static Connection conn;
    private static DataSource datasource;

    static {
        PoolProperties poolProperties = new PoolProperties();
        poolProperties.setUrl(Config.getInstance().getProperty(Config.URL));
        poolProperties.setDriverClassName(Config.getInstance().getProperty(Config.DRIVER));
        poolProperties.setUsername(Config.getInstance().getProperty(Config.USERNAME));
        poolProperties.setPassword(Config.getInstance().getProperty(Config.PASSWORD));
        poolProperties.setJmxEnabled(true);
        poolProperties.setTestWhileIdle(false);
        poolProperties.setTestOnBorrow(true);
        poolProperties.setTestOnReturn(false);
        poolProperties.setValidationQuery("SELECT 1");
        poolProperties.setValidationInterval(30000);
        poolProperties.setTimeBetweenEvictionRunsMillis(30000);
        poolProperties.setMaxActive(100);
        poolProperties.setInitialSize(10);
        poolProperties.setMaxWait(10000);
        poolProperties.setRemoveAbandonedTimeout(300);
        poolProperties.setMinEvictableIdleTimeMillis(30000);
        poolProperties.setMinIdle(10);
        poolProperties.setLogAbandoned(true);
        poolProperties.setRemoveAbandoned(true);
        poolProperties.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.ResetAbandonedTimer");
        datasource = new DataSource();
        datasource.setPoolProperties(poolProperties);
    }

    public static Connection getConnection() {
        try {
            conn = datasource.getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return conn;
    }
}