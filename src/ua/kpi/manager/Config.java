package ua.kpi.manager;

import java.util.ResourceBundle;


public class Config {
    private static Config instance;
    private ResourceBundle resource;
    private static final String BUNDLE_NAME = "resources.config";
    public static final String LOGIN_PAGE = "LOGIN_PAGE";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String USER_PAGE = "USER_PAGE";
    public static final String ADMIN_PAGE = "ADMIN_PAGE";
    public static final String URL = "URL";
    public static final String DRIVER = "DRIVER";

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
            instance.resource = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    public String getProperty(String key) {
        return (String) resource.getObject(key);
    }
}