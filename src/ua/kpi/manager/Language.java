package ua.kpi.manager;


import java.util.ResourceBundle;

public class Language {
    private ResourceBundle resource;
    private static Language instance;

    public static Language getInstance() {
        if (instance == null) {
            instance = new Language();
            instance.resource = ResourceBundle.getBundle("resources.data");
        }
        return instance;
    }

    public String getProperty(String key) {
        return (String) resource.getObject(key);
    }
}
