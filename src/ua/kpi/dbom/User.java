package ua.kpi.dbom;

import java.io.Serializable;


public class User implements Serializable {
    private int id;
    private String login;
    private String password;
    private boolean isBlocked;
    private double money;
    private int userTypeId;

    public User() {
    }

    public User(int id, String login, String password, boolean isBlocked, double money, int userTypeId) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.isBlocked = isBlocked;
        this.money = money;
        this.userTypeId = userTypeId;
    }

    public User(String login, String password, int userTypeId) {
        this.login = login;
        this.password = password;
        this.userTypeId = userTypeId;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.userTypeId = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", getIsBlocked=" + isBlocked +
                ", userTypeId=" + userTypeId +
                ", money=" + money +
                '}';
    }
}