package ua.kpi.dbom;


import java.io.Serializable;

public class UserHasService implements Serializable {
    private int id;
    private int usersId;
    private int servicesId;
    private boolean isServicePaid;

    public UserHasService() {
    }

    public UserHasService(int id, int usersId, int servicesId, boolean isServicePaid) {
        this.id = id;
        this.usersId = usersId;
        this.servicesId = servicesId;
        this.isServicePaid = isServicePaid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getIsServicePaid() {
        return isServicePaid;
    }

    public void setIsServicePaid(boolean servicePaid) {
        isServicePaid = servicePaid;
    }

    public int getUsersId() {
        return usersId;
    }

    public void setUsersId(int usersId) {
        this.usersId = usersId;
    }

    public int getServicesId() {
        return servicesId;
    }

    public void setServicesId(int servicesId) {
        this.servicesId = servicesId;
    }
}