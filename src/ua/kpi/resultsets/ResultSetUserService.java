package ua.kpi.resultsets;


public class ResultSetUserService {
    private int userHasServiceId;
    private String serviceName;
    private double serviceCost;
    private boolean isServicePaid;

    public ResultSetUserService() {
    }

    public ResultSetUserService(int userHasServiceId, String serviceName, double serviceCost, boolean isServicePaid) {
        this.userHasServiceId = userHasServiceId;
        this.serviceName = serviceName;
        this.serviceCost = serviceCost;
        this.isServicePaid = isServicePaid;
    }

    public int getUserHasServiceId() {
        return userHasServiceId;
    }

    public void setUserHasServiceId(int userHasServiceId) {
        this.userHasServiceId = userHasServiceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(double serviceCost) {
        this.serviceCost = serviceCost;
    }

    public boolean getIsServicePaid() {
        return isServicePaid;
    }

    public void setIsServicePaid(boolean servicePaid) {
        isServicePaid = servicePaid;
    }
}