package ua.kpi.resultsets;


import java.util.List;

public class ResultSetAdminPage {
    private int userId;
    private String login;
    private double balance;
    private int countUnpaidServices;
    private List<String> unpaidServices;
    private boolean isUserLock;

    public ResultSetAdminPage() {
    }

    public ResultSetAdminPage(int userId, String login, double balance, int countUnpaidServices, List<String> unpaidServices, boolean isUserLock) {
        this.userId = userId;
        this.login = login;
        this.balance = balance;
        this.countUnpaidServices = countUnpaidServices;
        this.unpaidServices = unpaidServices;
        this.isUserLock = isUserLock;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getCountUnpaidServices() {
        return countUnpaidServices;
    }

    public void setCountUnpaidServices(int countUnpaidServices) {
        this.countUnpaidServices = countUnpaidServices;
    }

    public List<String> getUnpaidServices() {
        return unpaidServices;
    }

    public void setUnpaidServices(List<String> unpaidServices) {
        this.unpaidServices = unpaidServices;
    }

    public boolean getIsUserLock() {
        return isUserLock;
    }

    public void setIsUserLock(boolean userLock) {
        isUserLock = userLock;
    }
}
