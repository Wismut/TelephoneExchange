package ua.kpi.dao.service;


import org.apache.log4j.Logger;
import ua.kpi.connection.ConnectionFactory;
import ua.kpi.connection.ConnectionUtil;
import ua.kpi.dbom.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceDAO implements IServiceDAO {
    private Connection conn = null;
    private final String INSERT = "INSERT INTO service VALUES(?, ?)";
    private final String FIND_BY_ID = "SELECT * FROM service WHERE id = ?";
    private final String FIND_BY_NAME = "SELECT * FROM service WHERE name = ?";
    private final String FIND_ALL = "SELECT * FROM service";
    private final Logger logger = Logger.getLogger(ServiceDAO.class);

    @Override
    public void insert(String name, double cost) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(INSERT);
            ps.setString(1, name);
            ps.setDouble(2, cost);
            ps.execute();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public Service findById(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Service service = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                service = new Service(rs.getInt(1), rs.getString(2), rs.getDouble(3));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return service;
    }

    @Override
    public Service findByName(String name) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Service service = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_NAME);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                service = new Service(rs.getInt(1), rs.getString(2), rs.getDouble(3));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return service;
    }

    @Override
    public List<Service> findAll() {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Service service;
        List<Service> services = new ArrayList<>();
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                service = new Service(rs.getInt(1), rs.getString(2), rs.getDouble(3));
                services.add(service);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return services;
    }
}