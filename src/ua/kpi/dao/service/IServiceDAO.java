package ua.kpi.dao.service;


import ua.kpi.dbom.Service;

import java.util.List;

public interface IServiceDAO {
    void insert(String name, double cost);

    Service findById(int id);

    Service findByName(String name);

    List<Service> findAll();
}