package ua.kpi.dao.factory;

public enum EDAOType {
    User("User"),
    UserType("UserType"),
    UserHasService("UserHasService"),
    Service("Service");

    private String id;

    EDAOType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}