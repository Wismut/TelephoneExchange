package ua.kpi.dao.factory;


import ua.kpi.dao.service.ServiceDAO;
import ua.kpi.dao.user.UserDAO;
import ua.kpi.dao.userhasservice.UserHasServiceDAO;
import ua.kpi.dao.usertype.UserTypeDAO;


public class DAOFactory {
    public static <T> T getDAO(EDAOType type) {
        switch (type) {
            case User:
                return (T) new UserDAO();
            case UserType:
                return (T) new UserTypeDAO();
            case UserHasService:
                return (T) new UserHasServiceDAO();
            case Service:
                return (T) new ServiceDAO();
            default:
                return null;
        }
    }
}