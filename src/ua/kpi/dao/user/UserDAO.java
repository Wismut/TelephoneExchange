package ua.kpi.dao.user;

import org.apache.log4j.Logger;
import ua.kpi.connection.ConnectionFactory;
import ua.kpi.connection.ConnectionUtil;
import ua.kpi.dao.factory.DAOFactory;
import ua.kpi.dao.factory.EDAOType;
import ua.kpi.dao.service.ServiceDAO;
import ua.kpi.dbom.Service;
import ua.kpi.dbom.User;
import ua.kpi.resultsets.ResultSetUserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements IUserDAO {
    private Connection conn = null;
    private static final Logger logger = Logger.getLogger(UserDAO.class);
    private final String UPDATE_MONEY = "UPDATE user SET money = ? WHERE id = ?";
    private final String FIND_MONEY_BY_ID = "SELECT money FROM user WHERE id = ?";
    private final String INSERT = "INSERT INTO user(login, password, user_type_id) VALUES (?,?,?)";
    private final String DELETE_BY_ID = "DELETE FROM user WHERE id = ?";
    private final String FIND_BY_LOGIN = "SELECT * FROM user WHERE login = ?";
    private final String FIND_BY_LOGIN_PASSWORD = "SELECT * FROM user WHERE login=? AND password=?";
    private final String UPDATE = "UPDATE user SET login=?,password=?,is_blocked=?,user_type_id=?,money=? WHERE id=?";
    private final String FIND_ALL = "SELECT * FROM user";
    private final String FIND_BY_ID = "SELECT * FROM user WHERE id = ?";
    private final String UPDATE_BLOCKED_STATUS = "UPDATE user SET is_blocked = ? WHERE id = ?";
    private final String FIND_SERVICES_BY_ID = "SELECT user_has_service.id, service.name, service.cost, user_has_service.is_service_paid FROM service, user_has_service WHERE user_has_service.service_id = service.id AND user_has_service.user_id = ?";
    private final String FIND_SERVICES_ID_BY_USER_ID = "SELECT service_id FROM user_has_service WHERE user_id = ?";
    private final int ADMIN_TYPE_ID = 2;

    public UserDAO() {
    }

    @Override
    public void insert(User user) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(INSERT);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setInt(3, user.getUserTypeId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public void updateBlockedStatus(int id, boolean blockingStatus) {
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_BLOCKED_STATUS)) {
            ps.setBoolean(1, blockingStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void update(User user) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(UPDATE);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setBoolean(3, user.getIsBlocked());
            ps.setInt(4, user.getUserTypeId());
            ps.setDouble(5, user.getMoney());
            ps.setInt(6, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public void delete(int id) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(DELETE_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public User findByLogin(String login) {
        User user = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_LOGIN);
            ps.setString(1, login);
            rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4), rs.getDouble(5), rs.getInt(6));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return user;
    }

    @Override
    public User findByLoginPassword(String login, String password) {
        User user = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_LOGIN_PASSWORD);
            ps.setString(1, login);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4), rs.getDouble(5), rs.getInt(6));
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setIsBlocked(rs.getBoolean(4));
                user.setMoney(rs.getDouble(5));
                user.setUserTypeId(rs.getInt(6));
                list.add(user);
            }
        } catch (Exception e) {
            logger.error(e);
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return list;
    }

    @Override
    public List<ResultSetUserService> findServicesById(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSetUserService userService;
        List<ResultSetUserService> userServices = new ArrayList<>();
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_SERVICES_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                userService = new ResultSetUserService(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getBoolean(4));
                userServices.add(userService);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return userServices;
    }

    @Override
    public List<Integer> findServicesIdByUserId(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Integer> servicesId = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_SERVICES_ID_BY_USER_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            servicesId = new ArrayList<>();
            while (rs.next()) {
                servicesId.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return servicesId;
    }

    @Override
    public List<Integer> findServicesThatCanBeSelectedById(int id) {
        ServiceDAO serviceDAO;
        UserDAO userDAO;
        List<Service> serviceList;
        List<Integer> allServicesId = null;
        try {
            serviceDAO = DAOFactory.getDAO(EDAOType.Service);
            serviceList = serviceDAO.findAll();
            allServicesId = new ArrayList<>();
            for (Service service : serviceList) {
                allServicesId.add(service.getId());
            }
            userDAO = DAOFactory.getDAO(EDAOType.User);
            List<Integer> userServicesId = userDAO.findServicesIdByUserId(id);
            allServicesId.removeAll(userServicesId);
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
        }
        return allServicesId;
    }

    @Override
    public void changeMoney(int id, double money) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(UPDATE_MONEY);
            ps.setDouble(1, money);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public double findMoneyById(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        double money = 0;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_MONEY_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                money = rs.getDouble(1);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return money;
    }

    @Override
    public boolean isAdmin(User user) {
        if (user == null) {
            try {
                throw new Exception();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } else {
            return user.getUserTypeId() == ADMIN_TYPE_ID;
        }
        return false;
    }

    @Override
    public User findById(int id) {
        User user = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setIsBlocked(rs.getBoolean(4));
                user.setMoney(rs.getDouble(5));
                user.setUserTypeId(rs.getInt(6));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return user;
    }
}