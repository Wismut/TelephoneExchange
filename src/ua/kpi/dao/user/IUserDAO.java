package ua.kpi.dao.user;

import ua.kpi.dbom.User;
import ua.kpi.resultsets.ResultSetUserService;

import java.util.List;

public interface IUserDAO {

    void insert(User user);

    void delete(int id);

    void updateBlockedStatus(int id, boolean blockingStatus);

    void update(User user);

    User findByLogin(String login);

    User findByLoginPassword(String login, String password);

    boolean isAdmin(User user);

    User findById(int id);

    List<User> findAll();

    List<ResultSetUserService> findServicesById(int id);

    List<Integer> findServicesIdByUserId(int id);

    List<Integer> findServicesThatCanBeSelectedById(int id);

    void changeMoney(int id, double money);

    double findMoneyById(int id);
}