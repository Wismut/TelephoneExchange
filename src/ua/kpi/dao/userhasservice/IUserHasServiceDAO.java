package ua.kpi.dao.userhasservice;

import ua.kpi.dbom.UserHasService;

import java.util.List;

public interface IUserHasServiceDAO {
    void insert(int userId, int serviceId);

    void delete(int userId, int serviceId);

    List<UserHasService> findByUserId(int userId);

    void payService(int id);

    boolean isServicePaid(int id);

    UserHasService findById(int id);

    List<UserHasService> findUnpaidServicesByUserId(int id);
}