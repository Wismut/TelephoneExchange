package ua.kpi.dao.userhasservice;


import org.apache.log4j.Logger;
import ua.kpi.connection.ConnectionFactory;
import ua.kpi.connection.ConnectionUtil;
import ua.kpi.dbom.UserHasService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserHasServiceDAO implements IUserHasServiceDAO {
    private Connection conn = null;
    private final Logger logger = Logger.getLogger(UserHasServiceDAO.class);
    private final String INSERT = "INSERT INTO user_has_service(user_id,service_id) VALUES(?,?)";
    private final String DELETE = "DELETE FROM user_has_service WHERE user_id = ? AND service_id = ?";
    private final String FIND_BY_USER_ID = "SELECT * FROM user_has_service WHERE user_id = ?";
    private final String PAY_SERVICE = "UPDATE user_has_service SET is_service_paid = true WHERE id = ?";
    private final String IS_SERVICE_PAID = "SELECT is_service_paid FROM user_has_service WHERE id = ?";
    private final String FIND_BY_ID = "SELECT * FROM user_has_service WHERE id = ?";
    private final String FIND_UNPAID_SERVICES_BY_ID = "SELECT * FROM user_has_service WHERE user_id = ? AND is_service_paid = false";
    private final String FIND_ALL_UNPAID_SERVICES = "SELECT * FROM user_has_service WHERE is_service_paid = false";
    private final String FIND_UNPAID_SERVICES = "SELECT user.id, user.login, user.money, user.is_blocked";


    @Override
    public void insert(int userId, int serviceId) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(INSERT);
            ps.setInt(1, userId);
            ps.setInt(2, serviceId);
            ps.execute();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public void delete(int userId, int serviceId) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(DELETE);
            ps.setInt(1, userId);
            ps.setInt(2, serviceId);
            ps.execute();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public List<UserHasService> findByUserId(int userId) {
        List<UserHasService> services = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserHasService userHasService = new UserHasService();
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_USER_ID);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                userHasService.setId(rs.getInt(1));
                userHasService.setUsersId(rs.getInt(2));
                userHasService.setServicesId(rs.getInt(3));
                userHasService.setIsServicePaid(rs.getBoolean(4));
                services.add(userHasService);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return services;
    }

    @Override
    public void payService(int id) {
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(PAY_SERVICE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
    }

    @Override
    public boolean isServicePaid(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean isServicePaid = false;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(IS_SERVICE_PAID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                isServicePaid = rs.getBoolean(1);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return isServicePaid;
    }

    @Override
    public UserHasService findById(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        UserHasService userHasService = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                userHasService = new UserHasService(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getBoolean(4));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return userHasService;
    }

    @Override
    public List<UserHasService> findUnpaidServicesByUserId(int id) {
        List<UserHasService> userHasServiceList;
        userHasServiceList = new ArrayList<>();
        UserHasService userHasService;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_UNPAID_SERVICES_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                userHasService = new UserHasService(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getBoolean(4));
                userHasServiceList.add(userHasService);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return userHasServiceList;
    }

    public Map<Integer, ArrayList<Integer>> findAllUnpaidServices() {
        Map<Integer, ArrayList<Integer>> unpaidServices = new HashMap<>();
        ArrayList<Integer> temp;
        temp = new ArrayList<>();
        UserHasService userHasService;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_ALL_UNPAID_SERVICES);
            rs = ps.executeQuery();
            while (rs.next()) {
                userHasService = new UserHasService(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getBoolean(4));
                if (!userHasService.getIsServicePaid()) {
                    if (unpaidServices.containsKey(userHasService.getUsersId())) {
                        temp = unpaidServices.get(userHasService.getUsersId());
                        temp.add(userHasService.getServicesId());
                        unpaidServices.put(userHasService.getUsersId(), temp);
                    } else {
                        temp.add(userHasService.getServicesId());
                        unpaidServices.put(userHasService.getUsersId(), temp);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return unpaidServices;
    }
}