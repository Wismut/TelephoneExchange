package ua.kpi.dao.usertype;


import org.apache.log4j.Logger;
import ua.kpi.connection.ConnectionFactory;
import ua.kpi.connection.ConnectionUtil;
import ua.kpi.dbom.UserType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserTypeDAO implements IUserTypeDAO {
    private Connection conn;
    private final Logger logger = Logger.getLogger(UserTypeDAO.class);
    private final String FIND_BY_ID = "SELECT * FROM user_type WHERE id = ?";
    private final String FIND_BY_NAME = "SELECT * FROM user_type WHERE name = ?";
    private final String FIND_ALL = "SELECT * FROM user_type";

    @Override
    public String findById(int id) {
        String name = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                name = rs.getString(2);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return name;
    }

    @Override
    public int findByName(String name) {
        int id = 0;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_BY_NAME);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return id;
    }

    @Override
    public List<UserType> getAll() {
        UserType userType;
        List<UserType> userTypes = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            conn = ConnectionFactory.getConnection();
            ps = conn.prepareStatement(FIND_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                userType = new UserType(rs.getInt(1), rs.getString(2));
                userTypes.add(userType);
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            ConnectionUtil.closeResultSet(rs);
            ConnectionUtil.closePreparedStatement(ps);
            ConnectionUtil.closeConnection(conn);
        }
        return userTypes;
    }
}