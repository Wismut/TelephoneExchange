package ua.kpi.dao.usertype;


import ua.kpi.dbom.UserType;

import java.util.List;

public interface IUserTypeDAO {
    String findById(int id);

    int findByName(String name);

    List<UserType> getAll();
}