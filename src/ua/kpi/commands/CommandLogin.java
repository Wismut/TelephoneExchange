package ua.kpi.commands;

import org.apache.log4j.Logger;
import ua.kpi.dao.factory.DAOFactory;
import ua.kpi.dao.factory.EDAOType;
import ua.kpi.dao.service.ServiceDAO;
import ua.kpi.dao.user.UserDAO;
import ua.kpi.dao.userhasservice.UserHasServiceDAO;
import ua.kpi.dbom.Service;
import ua.kpi.dbom.User;
import ua.kpi.dbom.UserHasService;
import ua.kpi.manager.Config;
import ua.kpi.manager.Language;
import ua.kpi.resultsets.ResultSetAdminPage;
import ua.kpi.resultsets.ResultSetUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CommandLogin implements ICommand {
    private final String USER_LOGIN = "login";
    private final String USER_PASSWORD = "password";
    private String page = null;
    private String userLogin;
    private String userPassword;
    private UserDAO userDAO;
    private String welcome;
    private String balance;
    private String login;
    private String password;
    private String payService;
    private String pay;
    private String youHaveNextServices;
    private String youCanSelectNextServices;
    private String enterLoginAndPassword;
    private String select;
    private String name;
    private String cost;
    private String logout;
    private String selectService;
    private String usersList;
    private String block;
    private String lockStatus;
    private String lockButton;
    private String numberOfUnpaidServices;
    private String unpaidServicesList;
    private String no;
    private String yes;
    private String blocked;
    private String servicePaid;
    private User user;
    private HttpSession session;
    private final Logger logger = Logger.getLogger(CommandLogin.class);


    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            session = request.getSession();
            session.setMaxInactiveInterval(10 * 60);
            userLogin = request.getParameter(USER_LOGIN);
            userPassword = request.getParameter(USER_PASSWORD);
            userDAO = DAOFactory.getDAO(EDAOType.User);
            user = userDAO.findByLoginPassword(userLogin, userPassword);
            if (user == null) {
                page = Config.getInstance().getProperty(Config.LOGIN_PAGE);
            } else if (user.getIsBlocked()) {
                page = Config.getInstance().getProperty(Config.LOGIN_PAGE);
            } else if (userDAO.isAdmin(user)) {
                page = getAdminPage();
            } else {
                page = getUserPage();
            }
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
        }
        return page;
    }

    private String getUserPage() {
        List<ResultSetUserService> userServices;
        ServiceDAO serviceDAO;
        List<Service> allServices;
        userServices = userDAO.findServicesById(user.getId());
        serviceDAO = DAOFactory.getDAO(EDAOType.Service);
        allServices = serviceDAO.findAll();
        session.setAttribute("userServices", userServices);
        pay = Language.getInstance().getProperty("PAY");
        youHaveNextServices = Language.getInstance().getProperty("YOU_HAVE_NEXT_SERVICES");
        youCanSelectNextServices = Language.getInstance().getProperty("YOU_CAN_SELECT_NEXT_SERVICES");
        login = Language.getInstance().getProperty("LOGIN");
        password = Language.getInstance().getProperty("PASSWORD");
        enterLoginAndPassword = Language.getInstance().getProperty("ENTER_LOGIN_AND_PASSWORD");
        welcome = Language.getInstance().getProperty("WELCOME");
        balance = Language.getInstance().getProperty("YOU_BALANCE_IS");
        select = Language.getInstance().getProperty("SELECT");
        name = Language.getInstance().getProperty("NAME");
        cost = Language.getInstance().getProperty("COST");
        logout = Language.getInstance().getProperty("LOGOUT");
        payService = Language.getInstance().getProperty("PAY_SERVICE");
        selectService = Language.getInstance().getProperty("SELECT_SERVICE");
        servicePaid = Language.getInstance().getProperty("SERVICE_PAID");
        session.setAttribute("pay", pay);
        session.setAttribute("youCanSelectNextServices", youCanSelectNextServices);
        session.setAttribute("youHaveNextServices", youHaveNextServices);
        session.setAttribute("user", user);
        session.setAttribute("allServices", allServices);
        session.setAttribute("login", login);
        session.setAttribute("password", password);
        session.setAttribute("enterLoginPass", enterLoginAndPassword);
        session.setAttribute("welcome", welcome);
        session.setAttribute("balance", balance);
        session.setAttribute("select", select);
        session.setAttribute("name", name);
        session.setAttribute("cost", cost);
        session.setAttribute("logout", logout);
        session.setAttribute("payService", payService);
        session.setAttribute("selectService", selectService);
        session.setAttribute("servicePaid", servicePaid);
        page = Config.getInstance().getProperty(Config.USER_PAGE);
        return page;
    }

    private String getAdminPage() {
        List<User> allUsers;
        UserHasServiceDAO userHasServiceDAO;
        ServiceDAO serviceDAO;
        allUsers = userDAO.findAll();
        userHasServiceDAO = DAOFactory.getDAO(EDAOType.UserHasService);
        serviceDAO = DAOFactory.getDAO(EDAOType.Service);
        List<ResultSetAdminPage> resultSetAdminPages = new ArrayList<>();
        ResultSetAdminPage resultSetAdminPage;
        for (User currentUser : allUsers) {
            resultSetAdminPage = new ResultSetAdminPage();
            resultSetAdminPage.setUserId(currentUser.getId());
            resultSetAdminPage.setLogin(currentUser.getLogin());
            resultSetAdminPage.setBalance(currentUser.getMoney());
            resultSetAdminPage.setIsUserLock(currentUser.getIsBlocked());
            List<UserHasService> unpaidServices = userHasServiceDAO.findUnpaidServicesByUserId(currentUser.getId());
            List<Service> services = new ArrayList<>();
            List<String> unpaidServiceNames = new ArrayList<>();
            for (UserHasService userHasService : unpaidServices) {
                services.add(serviceDAO.findById(userHasService.getServicesId()));
            }
            for (Service service : services) {
                unpaidServiceNames.add(service.getName());
            }
            resultSetAdminPage.setUnpaidServices(unpaidServiceNames);
            resultSetAdminPage.setCountUnpaidServices(unpaidServiceNames.size());
            resultSetAdminPages.add(resultSetAdminPage);
        }
        yes = Language.getInstance().getProperty("YES");
        no = Language.getInstance().getProperty("NO");
        usersList = Language.getInstance().getProperty("USERS_LIST");
        welcome = Language.getInstance().getProperty("WELCOME");
        balance = Language.getInstance().getProperty("BALANCE");
        login = Language.getInstance().getProperty("LOGIN");
        block = Language.getInstance().getProperty("BLOCK");
        lockStatus = Language.getInstance().getProperty("LOCK_STATUS");
        lockButton = Language.getInstance().getProperty("LOCK_BUTTON");
        logout = Language.getInstance().getProperty("LOGOUT");
        blocked = Language.getInstance().getProperty("BLOCKED");
        numberOfUnpaidServices = Language.getInstance().getProperty("NUMBER_OF_UNPAID_SERVICES");
        unpaidServicesList = Language.getInstance().getProperty("UNPAID_SERVICES_LIST");
        session.setAttribute("no", no);
        session.setAttribute("yes", yes);
        session.setAttribute("users", resultSetAdminPages);
        session.setAttribute("admin", user);
        session.setAttribute("usersList", usersList);
        session.setAttribute("welcome", welcome);
        session.setAttribute("balance", balance);
        session.setAttribute("login", login);
        session.setAttribute("block", block);
        session.setAttribute("lockStatus", lockStatus);
        session.setAttribute("lockButton", lockButton);
        session.setAttribute("logout", logout);
        session.setAttribute("blocked", blocked);
        session.setAttribute("numberOfUnpaidServices", numberOfUnpaidServices);
        session.setAttribute("unpaidServicesList", unpaidServicesList);
        page = Config.getInstance().getProperty(Config.ADMIN_PAGE);
        return page;
    }
}