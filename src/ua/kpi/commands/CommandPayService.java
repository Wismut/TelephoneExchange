package ua.kpi.commands;


import org.apache.log4j.Logger;
import ua.kpi.dao.factory.DAOFactory;
import ua.kpi.dao.factory.EDAOType;
import ua.kpi.dao.service.ServiceDAO;
import ua.kpi.dao.user.UserDAO;
import ua.kpi.dao.userhasservice.UserHasServiceDAO;
import ua.kpi.dbom.Service;
import ua.kpi.dbom.User;
import ua.kpi.dbom.UserHasService;
import ua.kpi.manager.Config;
import ua.kpi.resultsets.ResultSetUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class CommandPayService implements ICommand {
    private Logger logger = Logger.getLogger(CommandPayService.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        HttpSession session;
        User user;
        int userId;
        UserDAO userDAO;
        ServiceDAO serviceDAO;
        int userHasServiceId;
        UserHasServiceDAO userHasServiceDAO;
        UserHasService userHasService;
        List<ResultSetUserService> userServices;
        Service service;
        double userBalance;
        double newBalance;
        int servicesId;
        try {
            session = request.getSession();
            userHasServiceDAO = DAOFactory.getDAO(EDAOType.UserHasService);
            serviceDAO = DAOFactory.getDAO(EDAOType.Service);
            userHasServiceId = Integer.parseInt(request.getParameter("pay_service"));
            userDAO = DAOFactory.getDAO(EDAOType.User);
            user = (User) session.getAttribute("user");
            userId = user.getId();
            userBalance = userDAO.findMoneyById(userId);
            userHasService = userHasServiceDAO.findById(userHasServiceId);
            servicesId = userHasService.getServicesId();
            service = serviceDAO.findById(servicesId);
            newBalance = userBalance - service.getCost();
            if (!userHasServiceDAO.isServicePaid(userHasServiceId) && newBalance >= 0) {
                userHasServiceDAO.payService(userHasServiceId);
                userDAO.changeMoney(userId, newBalance);
                userServices = userDAO.findServicesById(userId);
                user = userDAO.findById(userId);
                session.setAttribute("userServices", userServices);
                session.setAttribute("user", user);
            }
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
        }
        page = Config.getInstance().getProperty(Config.USER_PAGE);
        return page;
    }
}