package ua.kpi.commands;

import ua.kpi.dao.factory.DAOFactory;
import ua.kpi.dao.factory.EDAOType;
import ua.kpi.dao.user.UserDAO;
import ua.kpi.dbom.User;
import ua.kpi.manager.Config;
import ua.kpi.resultsets.ResultSetAdminPage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


public class CommandUserBlocking implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        HttpSession session;
        List<ResultSetAdminPage> allUsers;
        session = request.getSession();
        int id = Integer.parseInt(request.getParameter("block"));
        UserDAO userDAO = DAOFactory.getDAO(EDAOType.User);
        User user = userDAO.findById(id);
        boolean isBlocked = user.getIsBlocked();
        userDAO.updateBlockedStatus(id, !isBlocked);
        allUsers = (List<ResultSetAdminPage>) session.getAttribute("users");
        for (ResultSetAdminPage currentUser : allUsers) {
            if (currentUser.getUserId() == id) {
                currentUser.setIsUserLock(!isBlocked);
                break;
            }
        }
        session.setAttribute("allUsers", allUsers);
        page = Config.getInstance().getProperty(Config.ADMIN_PAGE);
        return page;
    }
}