package ua.kpi.commands;

import ua.kpi.manager.Config;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CommandLogout implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        request.getSession().invalidate();
        page = Config.getInstance().getProperty(Config.LOGIN_PAGE);
        return page;
    }
}