/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.kpi.commands;


import ua.kpi.dbom.User;
import ua.kpi.manager.Config;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CommandMissing implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("user");
        if (request.getSession().getAttribute(user.getLogin()) == null) {
            Cookie cookie = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie element : cookies) {
                    if (element.getName().equals(user.getLogin())) {
                        cookie = element;
                        break;
                    }
                }
            }
            if (cookie != null) {
                request.getSession().setAttribute(user.getLogin(), cookie.getValue());
                return Config.getInstance().getProperty(Config.ADMIN_PAGE);
            }
            return Config.getInstance().getProperty(Config.LOGIN_PAGE);
        } else {
            return Config.getInstance().getProperty(Config.LOGIN_PAGE);
        }
    }
}