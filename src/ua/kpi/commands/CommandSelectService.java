package ua.kpi.commands;

import org.apache.log4j.Logger;
import ua.kpi.dao.factory.DAOFactory;
import ua.kpi.dao.factory.EDAOType;
import ua.kpi.dao.user.UserDAO;
import ua.kpi.dao.userhasservice.UserHasServiceDAO;
import ua.kpi.dbom.User;
import ua.kpi.manager.Config;
import ua.kpi.resultsets.ResultSetUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class CommandSelectService implements ICommand {
    private final String SELECT_SERVICE = "select_service";
    private final String USER = "user";
    private final Logger logger = Logger.getLogger(CommandSelectService.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        UserHasServiceDAO userHasServiceDAO;
        UserDAO userDAO;
        HttpSession session;
        List<ResultSetUserService> userServices;
        User user;
        int serviceId;
        try {
            session = request.getSession();
            userHasServiceDAO = DAOFactory.getDAO(EDAOType.UserHasService);
            serviceId = Integer.parseInt(request.getParameter(SELECT_SERVICE));
            userDAO = DAOFactory.getDAO(EDAOType.User);
            user = (User) session.getAttribute(USER);
            userHasServiceDAO.insert(user.getId(), serviceId);
            userServices = userDAO.findServicesById(user.getId());
            session.setAttribute("userServices", userServices);
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
        }
        page = Config.getInstance().getProperty(Config.USER_PAGE);
        return page;
    }
}