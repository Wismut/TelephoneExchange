<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Hello, User</title>
</head>
<body>
${welcome}, ${user.login}!
<br>
${balance} ${user.money}
<br><br>
${youHaveNextServices}
<br>
<table border="1">
    <tr>
        <td>${name}</td>
        <td>${cost}</td>
        <td>${servicePaid}</td>
        <td>${payService}</td>
    </tr>
    <c:forEach var="userService" items="${userServices}">
        <tr>
            <td><c:out value="${userService.serviceName}"/></td>
            <td><c:out value="${userService.serviceCost}"/></td>
            <td><c:out value="${userService.isServicePaid}"/></td>
            <td>
                <form action="controller" method="post">
                    <input type="hidden" name="command" value="pay_service"/>
                    <button name="pay_service" value="${userService.userHasServiceId}">${pay}</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
${youCanSelectNextServices}
<br>
<table border="1">
    <tr>
        <td>${name}</td>
        <td>${cost}</td>
        <td>${selectService}</td>
    </tr>
    <c:forEach var="service" items="${allServices}">
        <tr>
            <td><c:out value="${service.name}"/></td>
            <td><c:out value="${service.cost}"/></td>
            <td>
                <form action="controller" method="post">
                    <input type="hidden" name="command" value="select_service"/>
                    <button name="select_service" value="${service.id}">${select}</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<form method="post" action="controller">
    <input type="hidden" name="command" value="logout"/>
    <button name="logout">${logout}</button>
</form>
</body>
</html>