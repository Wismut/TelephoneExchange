<%--
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Admin panel</title>
</head>
<body>
${welcome}, ${admin.login}!
<br>
<br>
${usersList}
<br>
<table border="1">
    <tr>
        <td>${login}</td>
        <td>${balance}</td>
        <td>${numberOfUnpaidServices}</td>
        <td>${unpaidServicesList}</td>
        <td>${blocked}</td>
        <td>${lockButton}</td>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><c:out value="${user.login}"/></td>
            <td><c:out value="${user.balance}"/></td>
            <td><c:out value="${user.countUnpaidServices}"/></td>
            <td>
                <label>
                    <select>
                        <c:forEach var="unpaidService" items="${user.unpaidServices}">
                            <option>${unpaidService}</option>
                        </c:forEach>
                    </select>
                </label>
            </td>
            <td>
                <c:if test="${user.isUserLock == false}">${no}</c:if>
                <c:if test="${user.isUserLock == true}">${yes}</c:if>
            </td>
            <td>
                <form action="controller" method="post">
                    <input type="hidden" name="command" value="block"/>
                    <button name="block" value="${user.userId}">${block}</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<form method="post" action="controller">
    <input type="hidden" name="command" value="logout"/>
    <button name="logout">${logout}</button>
</form>
</body>
</html>